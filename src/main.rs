#![feature(nll)]

#[macro_use] extern crate lazy_static;
#[macro_use] extern crate bany;
extern crate speak_lib;
extern crate winit;

mod tts;

mod aui;
use aui::{Message, View};

use std::collections::HashMap;
use winit::{Event, EventsLoop, WindowEvent, Window, ControlFlow, KeyboardInput, VirtualKeyCode, ElementState};

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct KeyInput {
    key: Option<VirtualKeyCode>,
    shift: bool,
    ctrl: bool,
    alt: bool,
    logo: bool,
    release: bool
}

impl KeyInput {
    fn build(key: Option<VirtualKeyCode>) -> KeyInput {
        KeyInput {
            key,
            shift: false,
            ctrl: false,
            alt: false,
            logo: false,
            release: false
        }
    }

    fn set_shift(&mut self, val: bool) -> &mut KeyInput {
        self.shift = val;
        self
    }
    fn set_ctrl(&mut self, val: bool) -> &mut KeyInput {
        self.ctrl = val;
        self
    }

    #[allow(dead_code)]
    fn set_alt(&mut self, val: bool) -> &mut KeyInput {
        self.alt = val;
        self
    }

    #[allow(dead_code)]
    fn set_logo(&mut self, val: bool) -> &mut KeyInput {
        self.logo = val;
        self
    }

    fn from_winit(key: KeyboardInput) -> KeyInput {
        KeyInput {
            key: key.virtual_keycode,
            shift: key.modifiers.shift,
            ctrl: key.modifiers.ctrl,
            alt: key.modifiers.alt,
            logo: key.modifiers.logo,
            release: key.state == ElementState::Released
        }
    }
}

fn main() {
    let mut events_loop = EventsLoop::new();
    let _window = Window::new(&events_loop);

    let mut bindings: HashMap<KeyInput, Box<FnMut() -> Box<Message>>> = HashMap::new();

    {
        use VirtualKeyCode::*;

        // Buffer commands
        bindings.insert(KeyInput::build(Some(Left)), Box::new(|| Box::new(aui::Left)));
        bindings.insert(KeyInput::build(Some(Right)), Box::new(|| Box::new(aui::Right)));
        bindings.insert(KeyInput::build(Some(Up)), Box::new(|| Box::new(aui::Up)));
        bindings.insert(KeyInput::build(Some(Down)), Box::new(|| Box::new(aui::Down)));
        bindings.insert(*KeyInput::build(Some(E)).set_ctrl(true), Box::new(|| Box::new(aui::EndOfLine)));

        // Editor Commands
        bindings.insert(*KeyInput::build(Some(O)).set_ctrl(true), Box::new(|| Box::new(aui::OpenRequest)));
        bindings.insert(*KeyInput::build(Some(S)).set_ctrl(true), Box::new(|| Box::new(aui::Save)));
        bindings.insert(*KeyInput::build(Some(S)).set_ctrl(true).set_shift(true), Box::new(|| Box::new(aui::SaveAsRequest)));

        // Tabber commands
        bindings.insert(*KeyInput::build(Some(T)).set_ctrl(true), Box::new(|| Box::new(aui::NewTab)));
        bindings.insert(*KeyInput::build(Some(W)).set_ctrl(true), Box::new(|| Box::new(aui::CloseTab)));
        bindings.insert(*KeyInput::build(Some(Tab)).set_ctrl(true), Box::new(|| Box::new(aui::NextTab)));
        bindings.insert(*KeyInput::build(Some(Tab)).set_ctrl(true).set_shift(true), Box::new(|| Box::new(aui::PrevTab)));

        // TTS commands
        bindings.insert(*KeyInput::build(Some(A)).set_ctrl(true), Box::new(|| Box::new(aui::ReadAll)));
        bindings.insert(*KeyInput::build(Some(L)).set_ctrl(true), Box::new(|| Box::new(aui::ReadLine)));
        bindings.insert(*KeyInput::build(Some(C)).set_ctrl(true), Box::new(|| Box::new(aui::ReadChar)));
        bindings.insert(*KeyInput::build(Some(I)).set_ctrl(true), Box::new(|| Box::new(aui::SayContext)));

        // Misc
        bindings.insert(KeyInput::build(Some(Return)), Box::new(|| Box::new('\n')));
        bindings.insert(KeyInput::build(Some(Back)), Box::new(|| Box::new(aui::Backspace)));
        bindings.insert(KeyInput::build(Some(Delete)), Box::new(|| Box::new(aui::Delete)));
    }

    let mut view = aui::App::default();

    events_loop.run_forever(|event| {
        match event {
            Event::WindowEvent { event: WindowEvent::CloseRequested, .. } => {
                return ControlFlow::Break;
            },
            Event::WindowEvent { event: WindowEvent::KeyboardInput{ input: key, .. }, .. } => {
                if let Some(msg_builder) = bindings.get_mut(&KeyInput::from_winit(key)) {
                    let msg = msg_builder();
                    println!("Action: {:?}", msg);
                    view.send_message(msg);
                }
            },
            Event::WindowEvent { event: WindowEvent::ReceivedCharacter(c), .. } => {
                // TODO: Once this is merged: https://github.com/rust-lang/rust/pull/50045
                // it would be a good way to fight indentation here.
                if !c.is_control() {
                    println!("Char: {}", c);
                    view.send_message(Box::new(c));
                }
            }
            _ => ()
        };

        ControlFlow::Continue
    });
}