use speak_lib::TTS as SPEAK_LIB_TTS;

lazy_static! {
    pub static ref TTS: Tts = Tts {};
}

pub struct Tts {}

impl Tts {
    pub fn clear(&self) {
        println!("clear");
        SPEAK_LIB_TTS.cancel().expect("TTS: cancel failed");
    }

    pub fn say(&self, msg: &str) {
        println!("saying: {}", msg);
        SPEAK_LIB_TTS.synth(msg).expect("TTS: synth failed");
    }

    pub fn error(&self, msg: &str) {
        self.clear();
        self.say(msg)
    }
}