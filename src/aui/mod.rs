mod buffer;
mod components;

pub use self::components::*;
pub use self::buffer::{ReadAll, ReadLine, ReadChar, Left, Right, Up, Down, EndOfLine};

use tts::TTS;

pub trait View {
    fn short_info(&self) -> String;

    fn handle_message<'a>(&'a mut self, msg: Box<Message + 'static>) -> Option<Box<Message + 'static>> {
        Some(msg)
    }

    fn active_child(&mut self) -> Option<&mut View> {
        None
    }

    /// Not meant to be reimplemented
    fn send_message(&mut self, mut msg: Box<Message + 'static>) -> Option<Box<Message + 'static>> {
        if let Some(child) = self.active_child() {
            let msg_new = child.send_message(msg);
            match msg_new {
                Some(msg_new) => msg = msg_new,
                None => return None
            }
        }

        self.handle_message(msg)
    }

    /// Not meant to be reimplemented
    fn say_short_info_stack(&mut self) {
        if let Some(child) = self.active_child() {
            child.say_short_info_stack();
        }

        TTS.say(&self.short_info());
    }
}

pub trait Message: ::bany::GetTypeId + ::std::fmt::Debug {}

impl Message for char {}

#[derive(Debug)]
pub struct Backspace;
impl Message for Backspace {}

#[derive(Debug)]
pub struct Delete;
impl Message for Delete {}