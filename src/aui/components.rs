use std::fs;
use std::path::PathBuf;

use tts::TTS;
use aui::{
    Message, View,
    buffer::{Buffer, LineBuffer}
};

#[derive(Debug)]
pub struct OpenRequest;
impl Message for OpenRequest {}

#[derive(Debug)]
pub struct Open(PathBuf);
impl Message for Open {}

/// Save as the existing filename, or if not one treat as a `SaveAsRequest`
#[derive(Debug)]
pub struct Save;
impl Message for Save {}

/// Prompt for a filename to save as, and then save the file as it
#[derive(Debug)]
pub struct SaveAsRequest;
impl Message for SaveAsRequest {}

/// Save to path
#[derive(Debug)]
pub struct SaveAs(PathBuf);
impl Message for SaveAs {}

#[derive(Debug)]
pub struct SayContext;
impl Message for SayContext {}

#[derive(Default)]
pub struct App {
    contents: Tabber,
}

#[derive(Debug)]
pub struct NewTab;
impl Message for NewTab {}

#[derive(Debug)]
pub struct CloseTab;
impl Message for CloseTab {}

#[derive(Debug)]
pub struct NextTab;
impl Message for NextTab {}

#[derive(Debug)]
pub struct PrevTab;
impl Message for PrevTab {}

struct Tabber {
    tabs: Vec<Box<View>>,
    active: usize
}

#[derive(Default)]
pub struct Editor {
    file: Option<PathBuf>,
    prompt: Option<Prompt>,
    contents: Buffer
}

struct Prompt {
    msg: String,
    contents: LineBuffer
}

impl View for App {
    fn short_info(&self) -> String {
        "".into()
    }

    fn active_child(&mut self) -> Option<&mut View> {
        Some(&mut self.contents)
    }

    fn handle_message(&mut self, msg: Box<Message>) -> Option<Box<Message>> {
        dyn_ref_match!(msg;
            SayContext > _ => {
                self.say_short_info_stack();
                None
            },
            else Some(msg)
        )
    }
}

impl Default for Tabber {
    fn default() -> Tabber {
        Tabber {
            tabs: vec![Box::new(Editor::default())],
            active: 0
        }
    }
}

impl View for Tabber {
    fn short_info(&self) -> String {
        format!("Tab {} of {}.", self.active + 1, self.tabs.len())
    }

    fn active_child(&mut self) -> Option<&mut View> {
        Some(&mut *self.tabs[self.active])
    }

    fn handle_message(&mut self, msg: Box<Message>) -> Option<Box<Message>> {
        dyn_ref_match!(msg;
            NewTab > _ => {
                self.active += 1;
                self.tabs.insert(self.active, Box::new(Editor::default()));

                None
            },
            CloseTab > _ => {
                if self.tabs.len() == 1 {
                    self.tabs[0] = Box::new(Editor::default());
                }
                else {
                    self.tabs.remove(self.active);
                }

                None
            },
            NextTab > _ => {
                self.active = (self.active + 1) % self.tabs.len();
                TTS.clear();
                TTS.say(&format!("{}. Tab {} of {}.", self.tabs[self.active].short_info(), self.active + 1, self.tabs.len()));

                None
            },
            PrevTab > _ => {
                if self.active == 0 { self.active = self.tabs.len(); }
                self.active -= 1;

                TTS.clear();
                TTS.say(&format!("{}. Tab {} of {}.", self.tabs[self.active].short_info(), self.active + 1, self.tabs.len()));

                None
            },
            else Some(msg)
        )
    }
}

impl Editor {
    fn save(&self) -> Option<Box<Message>> {
        fs::write(self.file.as_ref().unwrap(), &self.contents.contents)
            .unwrap_or_else(|_| {
                TTS.error(&format!("Failed to write buffer to file at {:?}", self.file.as_ref().unwrap()))
            });

        None
    }

    fn save_as_request(&mut self) -> Option<Box<Message>> {
        let ret_fn = Box::new(|path_bytes: &[u8]| {
            use std::str;
            let path = match str::from_utf8(path_bytes) {
                Ok(x) => x,
                Err(_) => {
                    // TODO: This is an unfortunate restriction, but I don't
                    // see a fast cross platform way around it.
                    TTS.error("Invalid path - not utf8. Try again.");
                    return None;
                }
            };
            Some(Box::new(SaveAs(PathBuf::from(path))) as Box<Message>)
        });
        self.prompt = Some(Prompt::new("filename?".to_string(), ret_fn));

        None
    }

    fn open_request(&mut self) -> Option<Box<Message>> {
        let ret_fn = Box::new(|path_bytes: &[u8]| {
            use std::str;
            let path = match str::from_utf8(path_bytes) {
                Ok(x) => x,
                Err(_) => {
                    // TODO: This is an unfortunate restriction, but I don't
                    // see a fast cross platform way around it.
                    TTS.error("Invalid path - not utf8. Try again.");
                    return None;
                }
            };

            Some(Box::new(Open(PathBuf::from(path))) as Box<Message>)
        });
        self.prompt = Some(Prompt::new("filename?".to_string(), ret_fn));

        None
    }
}

impl View for Editor {
    fn short_info(&self) -> String {
        if let Some(ref file) = self.file {
            file.file_name().expect("no file name?").to_string_lossy().into()
        }
        else {
            "untitled editor".into()
        }
    }

    fn active_child(&mut self) -> Option<&mut View> {
        if self.prompt.is_some() {
            self.prompt.as_mut().map(|x| x as &mut View)
        }
        else {
            Some(&mut self.contents)
        }
    }

    fn handle_message(&mut self, msg: Box<Message>) -> Option<Box<Message + 'static>> {
        dyn_ref_match!(msg;
            Save > &Save => {
                if self.file.is_some() {
                    self.save()
                }
                else {
                    self.save_as_request()
                }
            },
            SaveAs > &SaveAs(ref path) => {
                // TODO: This is in theory an uncessary clone, think about how I
                // can improve bany to not require it.
                self.file = Some(path.clone());
                self.prompt = None;
                self.save()
            },
            SaveAsRequest > _ => self.save_as_request(),
            Open > &Open(ref path) => {
                self.contents.contents = match fs::read(path) {
                    Ok(x) => x,
                    Err(_) => {
                        // Note that the prompt is still open at this point.
                        TTS.error("Failed to read file. Try again.");
                        return None
                    }
                };
                self.contents.cursor = 0;
                self.file = Some(path.clone());
                self.prompt = None;

                TTS.say(&format!("Opened file {:?}.", path.file_name().unwrap()));

                None
            },
            OpenRequest > _ => self.open_request(),
            else Some(msg)
        )
    }
}

impl Prompt {
    fn new(msg: String, ret_fn: Box<FnMut(&[u8]) -> Option<Box<Message>>>) -> Prompt {
        TTS.clear();
        TTS.say(&msg);

        Prompt {
            msg,
            contents: LineBuffer::new(ret_fn)
        }
    }
}

impl View for Prompt {
    fn short_info(&self) -> String {
        format!("Prompt for {}.", self.msg.clone())
    }

    fn active_child(&mut self) -> Option<&mut View> {
        Some(&mut self.contents)
    }
}
