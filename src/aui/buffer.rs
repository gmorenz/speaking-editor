use tts::TTS;
use aui::{Message, View, Backspace, Delete};

/// This grows vec by `len`, moving everything after `pos` down by `len`, and leaving
/// uninitialized memory from `pos` to `pos + len`. Note that if the vec can be dropped
/// after this funciton before inserting elements there is undefined behaviour.
///
/// Note that if we drop the elements at `pos` to `pos + len` before initializing them,
/// and they are non copy, there is undefined behaviour.
unsafe fn vec_make_space<T>(v: &mut Vec<T>, pos: usize, len: usize) {
    v.reserve(len);
    let v_len = v.len();

    let p = v.as_mut_ptr().offset(pos as isize);
    ::std::ptr::copy(p, p.offset(len as isize), v_len - pos);

    v.set_len(v_len + len);
}

fn vec_write_char_utf8(v: &mut Vec<u8>, pos: usize, c: char) {
    unsafe {
        let len = c.len_utf8();
        vec_make_space(v, pos, len);
        c.encode_utf8(&mut v[pos.. pos + len]);
    }
}

#[derive(Debug)]
pub struct ReadAll;
impl Message for ReadAll {}

#[derive(Debug)]
pub struct ReadLine;
impl Message for ReadLine {}

#[derive(Debug)]
pub struct ReadChar;
impl Message for ReadChar {}

#[derive(Debug)]
pub struct Left;
impl Message for Left {}

#[derive(Debug)]
pub struct Right;
impl Message for Right {}

#[derive(Debug)]
pub struct Up;
impl Message for Up {}

#[derive(Debug)]
pub struct Down;
impl Message for Down {}

#[derive(Debug)]
pub struct EndOfLine;
impl Message for EndOfLine {}

#[derive(Default)]
pub struct Buffer {
    pub contents: Vec<u8>,
    pub cursor: usize,
}

pub struct LineBuffer {
    pub contents: Vec<u8>,
    pub cursor: usize,
    ret_fn: Box<FnMut(&[u8]) -> Option<Box<Message>>>
}

impl Buffer {
    fn start_of_line(&self, pos: usize) -> bool {
        pos == 0 || self.contents[pos - 1] == b'\n'
    }

    fn horiz_offset(&self) -> usize {
        let mut ret = 0;
        while !self.start_of_line(self.cursor - ret) {
            ret += 1;
        }
        ret
    }

    fn vert_offset(&self) -> usize {
        let mut ret = 0;
        for &c in &self.contents[0.. self.cursor] {
            if c == b'\n' {
                ret += 1;
            }
        }

        ret
    }
}

impl View for Buffer {
    fn handle_message(&mut self, msg: Box<Message>) -> Option<Box<Message + 'static>> {
        dyn_ref_match!(msg;
            char > &c => {
                println!("Buffer handle char: {:?}", c);
                vec_write_char_utf8(&mut self.contents, self.cursor, c);
                self.cursor += c.len_utf8();

                None
            },
            Backspace > _ => {
                if self.cursor == 0 {
                    TTS.error("Start of buffer.");
                }
                else {
                    self.cursor -= 1;
                    self.contents.remove(self.cursor);
                }
                None
            },
            Delete > _ => {
                if self.cursor == self.contents.len() {
                    TTS.error("End of buffer.");
                }
                else {
                    self.contents.remove(self.cursor);
                }
                None
            },
            ReadAll > _ => {
                let s = String::from_utf8_lossy(&self.contents);
                TTS.clear();
                TTS.say(&s);

                None
            },
            ReadLine > _ => {
                let mut start = self.cursor;
                while start > 0 && self.contents[start - 1] != b'\n' {
                    start -= 1;
                }
                let mut end = self.cursor;
                while end < self.contents.len() && self.contents[end] != b'\n' {
                    end += 1;
                }

                let s = String::from_utf8_lossy(&self.contents[start.. end]);
                TTS.clear();
                TTS.say(&s);

                None
            },
            ReadChar > _ => {
                TTS.clear();
                if self.cursor >= self.contents.len() {
                    TTS.say("End of buffer.");
                }
                else if let Some(char_buf) = get_utf8_char(&self.contents[self.cursor..]) {
                    TTS.say(&char_buf);
                }
                else {
                    TTS.say(&format!("Binary {}", self.contents[self.cursor]));
                }

                None
            },
            Right > _ => {
                self.cursor += 1;
                if self.cursor > self.contents.len() {
                    self.cursor = self.contents.len();
                    TTS.error("End of buffer.");
                }

                None
            },
            Left > _ => {
                if self.cursor > 0 {
                    self.cursor -= 1;
                }
                else {
                    TTS.error("Start of buffer.");
                }

                None
            },
            Up > _ => {
                let target_offset = self.horiz_offset();
                self.cursor -= self.horiz_offset();
                if self.cursor == 0 {
                    TTS.error("Start of buffer.");
                    return None;
                }
                self.cursor -= 1;
                let mut current_offset = self.horiz_offset();

                while current_offset > target_offset {
                    self.cursor -= 1;
                    current_offset -= 1;
                }

                self.handle_message(Box::new(ReadLine));

                None
            },
            Down > _ => {
                let target_offset = self.horiz_offset();
                self.cursor += 1;

                while self.cursor <= self.contents.len() && self.contents[self.cursor - 1] != b'\n' {
                    self.cursor += 1;
                }

                if self.cursor > self.contents.len() {
                    self.cursor = self.contents.len();
                    TTS.error("End of buffer.");
                    return None;
                }

                let mut current_offset = 0;
                while self.cursor < self.contents.len() && current_offset < target_offset {
                    self.cursor += 1;
                    current_offset += 1;
                }

                self.handle_message(Box::new(ReadLine));

                None
            },
            EndOfLine > _ => {
                while self.contents[self.cursor] != b'\n' {
                    self.cursor += 1;

                    if self.cursor == self.contents.len() {
                        TTS.error("End of buffer.");
                        return None;
                    }
                }

                None
            },
            else Some(msg)
        )
    }

    fn short_info(&self) -> String {
        format!("Line {}, Column {}.", self.vert_offset() + 1, self.horiz_offset() + 1)
    }
}

impl LineBuffer {
    pub fn new(ret_fn: Box<FnMut(&[u8]) -> Option<Box<Message>>>) -> LineBuffer {
        LineBuffer {
            contents: vec![],
            cursor: 0,
            ret_fn
        }
    }
}

impl View for LineBuffer {
    fn short_info(&self) -> String {
        format!("column {}.", self.cursor + 1)
    }

    fn handle_message(&mut self, msg: Box<Message + 'static>) -> Option<Box<Message + 'static>> {
        dyn_ref_match!{ msg;
            char > &c => {
                println!("linebuf handle char: {:?}", c);

                if c == '\n' {
                    if let Some(msg) = (self.ret_fn)(&self.contents) {
                        return Some(msg)
                    }
                }
                else {
                    vec_write_char_utf8(&mut self.contents, self.cursor, c);
                    self.cursor += c.len_utf8();
                }

                None
            },
            ReadAll > _ => {
                let s = String::from_utf8_lossy(&self.contents);
                TTS.clear();
                TTS.say(&s);

                None
            },
            ReadLine > _ => {
                let s = String::from_utf8_lossy(&self.contents);
                TTS.clear();
                TTS.say(&s);

                None
            },
            else Some(msg)
        }
    }
}

fn get_utf8_char(t: &[u8]) -> Option<&str> {
    use std::str;
    // TODO: Performance, I could really use an unchecked convert to str
    get_utf8_char_len(t).map(|len| str::from_utf8(&t[..len as usize]).unwrap())
}

fn get_utf8_char_len(t: &[u8]) -> Option<u8> {
    if t.len() == 0 { return None; }
    let c0 = t[0];
    if c0 & 0x80 == 0 {
        Some(1)
    }
    else if c0 & 0b0010_0000 == 0 &&
            c0 & 0b1100_0000 == 0b1100_0000 &&
            t.len() >= 2 {
        Some(2)
    }
    else if c0 & 0b0001_0000 == 0 &&
            c0 & 0b1110_0000 == 0b1110_0000 &&
            t.len() >= 3 {
        Some(3)
    }
    else if c0 & 0b0000_1000 == 0 &&
            c0 & 0b1111_0000 == 0b1111_0000 &&
            t.len() >= 4 {
        Some(4)
    }
    else {
        None
    }
}