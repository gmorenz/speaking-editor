# A speaking text editor.

A (WIP!) text editor whose only output is audio, instead of the normal tui/gui.

Designed for text to speech first, instead of designed for a graphical interface
and then retrofitted to add TTS, I hope to be able to design a simpler, more
intuitive interface.

## Controls

These are pretty much just placeholders right now

 - `Arrow keys` - Navigation
 - `Ctrl E` - End of line
 - `Ctrl O` - Open file
 - `Ctrl S` - Save file
 - `Ctrl Shift S` - Save file as
 - `Ctrl T` - New tab
 - `Ctrl W` - Close tab
 - `Ctrl Tab` - Next tab
 - `Ctrl Shift Tab` - Previous tab
 - `Ctrl A` - Read entire buffer
 - `Ctrl L` - Read current line
 - `Ctrl C` - Read character under cursor
 - `Ctrl I` - Read current location in the editor

## Similar tools

[emacspeak](http://emacspeak.sourceforge.net/) is the most similar thing that
I'm aware of. It is a very complete and very powerful environment, just like
normal emacs really. Unfortunately also just like normal emacs it's a very steep
learning curve. There are also components of it that just don't make much sense
when you are interacting without a screen, e.g. multiple panes.

A screen reader and a typical text editor. The same arguments for why
[emacspeak is an improvment](http://emacspeak.sourceforge.net/#benefits) should
apply here, as well as teh arguments for why this is better than emacspeak.

## Why

The canonical use case for something like this is the blind. I'm not blind, but
in my free time I do enjoy being outside, and I enjoy programming. Unfortunately
when it's sunny out being outside makes it so I have to strain my eyes to see
the screen. Primarily this is my attempt to work around that. I tried the
existing options and wasn't satisfied.

I also think that programming via TTS will force me to have a much clearer
mental model of my code, and result me in being a better programmer.
